package com.linecmd.linecmd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import com.google.gson.Gson;
import com.linecmd.service.dto.ThCardLocationDto;
import com.linecmd.service.dto.ThCardMessageDto;
import com.qizhi.dto.CardMessageDto;

public class ApiTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		testUploadlocation();
		testUploadCarmessage();
	}

 public static void testUploadlocation(){
	 ThCardLocationDto cardLocation = new ThCardLocationDto();
	 cardLocation.setAltitude(10d);
	 cardLocation.setCreatedTime("2016-02-16 11:20:23");
	 cardLocation.setDirection(10d);
	 cardLocation.setFromCardNumber("307175");
	 cardLocation.setLatitude(23.1234673);
	 cardLocation.setLongitude(113.324567);
	 cardLocation.setSpeed(23d);
		
		try{
		String uri="http://127.0.0.1:8081/bdcbus/bdcbus/pullBDThCardLocationDto.do";
		HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
		httpClient.getParams().setParameter(
				CoreConnectionPNames.SO_TIMEOUT, 6000);
		HttpPost httpPostReq = new HttpPost(uri);
		JSONObject obj = JSONObject.fromObject(cardLocation);
		String aaString = obj.toString();
		StringEntity s = new StringEntity(aaString,"utf-8");
		s.setContentEncoding("UTF-8");
		s.setContentType("application/json");
		httpPostReq.setEntity(s);
		HttpResponse resp = httpClient.execute(httpPostReq);
		if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity()
							.getContent()));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while ((inputLine = reader.readLine()) != null) {
				result.append(inputLine);
			}

		} else {
		}

	} catch (Exception e) {
		e.printStackTrace();
		}
	 }
 
 public static void testUploadCarmessage(){
	 ThCardMessageDto cardMessageDto = new ThCardMessageDto();
		cardMessageDto.setContent("你好");
		cardMessageDto.setCreatedTime("2016-02-16 11:20:23");
		cardMessageDto.setFromCardNumber("307175");
		cardMessageDto.setToCardNumber("307175");
		try{
		String uri="http://127.0.0.1:8081/bdcbus/bdcbus/pullBDThCardMessageDto.do";
		HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
		httpClient.getParams().setParameter(
				CoreConnectionPNames.SO_TIMEOUT, 6000);
		HttpPost httpPostReq = new HttpPost(uri);
		JSONObject obj =JSONObject.fromObject(cardMessageDto);
		
		String aaString = obj.toString();
		StringEntity s = new StringEntity(aaString,"utf-8");
		s.setContentEncoding("UTF-8");
		s.setContentType("application/json");
		httpPostReq.setEntity(s);
		HttpResponse resp = httpClient.execute(httpPostReq);
		if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity()
							.getContent()));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while ((inputLine = reader.readLine()) != null) {
				result.append(inputLine);
			}

		} else {
		}

	} catch (Exception e) {
		e.printStackTrace();
		}
 }
 
 
}
