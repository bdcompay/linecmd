package com.linecmd.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.linecmd.dao.LinedataDao;
import com.linecmd.entity.Linedata;
@Repository
public class LinedataDaoImpl extends IDaoImpl<Linedata, Long> implements LinedataDao {

	@Override
	public List<Linedata> getLinedataList(int page,int pageSize) {
		// TODO Auto-generated method stub
		return this.getList("from Linedata", page, pageSize, null);
		
	}
}
