package com.linecmd.dao;

import java.util.List;

import com.linecmd.entity.Linedata;

public interface LinedataDao extends IDao<Linedata, Long> {

	public List<Linedata> getLinedataList(int page,int pageSize);
}