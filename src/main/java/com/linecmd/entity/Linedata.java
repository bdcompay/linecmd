package com.linecmd.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "l_linedata")
public class Linedata {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="l_strdata",length=255)
	private String strdata;
	
	@Column(name="l_createdtime")
	private Calendar createdTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStrdata() {
		return strdata;
	}

	public void setStrdata(String strdata) {
		this.strdata = strdata;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	} 
	
}
