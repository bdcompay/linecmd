package com.linecmd.biz.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linecmd.biz.LinedataBiz;
import com.linecmd.dao.LinedataDao;
import com.linecmd.entity.Linedata;

/**
 * 专线数据业务层实现类
 * 
 * @author jlj
 *
 */
@Service
public class LinedataBizImpl implements LinedataBiz {
	@Autowired
	private LinedataDao linedataDao;

	
	
	@Override
	public void saveLinedata(Linedata linedata) {
		linedataDao.save(linedata);
	}

	@Override
	public Linedata get(long id) {
		return linedataDao.get(id);
	}

	@Override
	public boolean delete(long id) {
		try{
			linedataDao.delete(id);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void deleteLinedata(Linedata linedata) {
		linedataDao.deleteObject(linedata);
	}

	@Override
	public List<Linedata> getLinedataList(int page, int pageSize) {
		return linedataDao.getLinedataList(page, pageSize);
	}

	@Override
	public void updateLinedata(Linedata linedata) {
		linedataDao.update(linedata);
	}

}
