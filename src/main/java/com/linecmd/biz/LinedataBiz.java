package com.linecmd.biz;

import java.util.List;

import com.linecmd.entity.Linedata;

/**
 * 专线数据业务层接口
 * 
 * @author jlj
 *
 */
public interface LinedataBiz {

	/**
	 * 保存专线数据
	 * 
	 * @param linedata
	 * 		专线数据对象
	 */
	public void saveLinedata(Linedata linedata);
	
	/**
	 * 通过id获取专线数据对象
	 * 
	 * @param id
	 * 		专线数据id
	 * @return
	 */
	public Linedata get(long id);
	
	/**
	 * 通过id删除专线数据
	 * 
	 * @param id
	 * @return
	 */
	public boolean delete(long id);
	
	/**
	 * 通过专线数据对象删除专线数据
	 * 
	 * @param linedata
	 * 		专线数据对象
	 */
	public void deleteLinedata(Linedata linedata);
	
	/**
	 * 只通过分页查询多个专线数据信息
	 * 
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页记录数
	 * @return
	 */
	public List<Linedata> getLinedataList(int page,int pageSize);
	
	/**
	 * 根据专线数据修改
	 * 
	 * @param linedata
	 */
	public void updateLinedata(Linedata linedata);
}
