package com.linecmd.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bdsdk.util.BdCommonMethod;
import com.linecmd.biz.LinedataBiz;
import com.linecmd.entity.Linedata;
import com.linecmd.service.dto.ThCardLocationDto;
import com.linecmd.service.dto.ThCardMessageDto;
import com.linecmd.util.EncryptionByMD5;
import com.pro.Analysis;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;

@Component
public class THDataHandler extends IoHandlerAdapter {
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger fordebug = Logger.getLogger("fordebug");
	private static ArrayList<String> dataList = new ArrayList<String>(10000);
	@Autowired
	private SmackHandler smackHandler;
	@Autowired
	private LinedataBiz linedataBiz;
	@Value("${test.send}")
	private boolean testsend;
	@Value("${bdcbus.thlocationurl}")
	private String thlocationurl;
	@Value("${bdcbus.thmessageurl}")
	private String thlmessageurl;
	@Value("${bdth.LoginID}")
	private String LoginID;
	@Value("${bdth.SharedSecret}")
	private String SharedSecret;
	@Value("${bdth.Version}")
	private String Version;

	@PostConstruct
	public void init() {
		sendDbMsg();
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(session, cause);
	}

	@Override
	public void messageReceived(IoSession session, Object message)
			throws Exception {
		// TODO Auto-generated method stub
		IoBuffer ioBuffer = (IoBuffer) message;
		byte[] bytedata = ioBuffer.array();
		if (bytedata[0] != (byte) 0xFF && bytedata[1] != (byte) 0xFF) {
			return;
		}

		forinfo.info("收到天汇数据："
				+ Analysis.ByteHexToString(bytedata, bytedata.length));
		String Strbytedata = Analysis
				.ByteHexToString(bytedata, bytedata.length);
		String hexData = BdCommonMethod.castBytesToString(bytedata);
		// 定义取数据标志
		int length = 0;
		for (int i = 0; i < hexData.length(); i = i + length) {
			// 第一条数据长度16进制
			String hexlength = hexData.substring(i + 4, i + 12);
			// 第一条数据长度10进制
			length = (Integer.valueOf(hexlength, 16)) * 2;
			if (hexData.length() > length) {
				String s1 = hexData.substring(i, i + length);
				dealThdata(s1, session);
				forinfo.info("分包数据内容" + s1);

				String s2 = hexData.substring(length);
				if (!s2.startsWith("FFFF")) {
					//
					break;
				}
			} else if (hexData.length() == length) {
				dealThdata(hexData, session);
				forinfo.info("处理数据：" + hexData);
				break;
			} else {
				forerror.error("数据长度异常" + hexData);
				break;
			}
		}
		/*
		 * String[] hexDatas = hexData.split("FFFF"); if (hexDatas.length > 2) {
		 * System.out.println("分包处理"); for (int i = 1; i <= hexDatas.length;
		 * i++) { String s = "FFFF" + hexDatas[i]; dealThdata(s, session); } }
		 * else { dealThdata(hexData, session); }
		 */
	}

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		// TODO Auto-generated method stub
		// super.messageSent(session, message);
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionClosed(session);
	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionCreated(session);
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status)
			throws Exception {
		// TODO Auto-generated method stub
		super.sessionIdle(session, status);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionOpened(session);
	}

	/**
	 * 处理天汇数据
	 * 
	 * @param bytedata
	 * @param session
	 */
	public void dealThdata(String hexData, IoSession session) {
		// String hexData = BdCommonMethod.castBytesToString(bytedata);
		String FixedFlag = hexData.substring(0, 4);
		String Total_Length = hexData.substring(4, 12);
		String Command_Id = hexData.substring(12, 20);
		String Sequence_Id = hexData.substring(20, 28);
		String CompressionEncryption = hexData.substring(28, 30);
		String Status = hexData.substring(30, 32);
		String Reserved = hexData.substring(32, 36);
		// 请求链接
		if (Command_Id.equals("80000001")) {
			String messageBody = hexData.substring(36);
			String respMessageBody = dealConnect(messageBody);
			String respStr = resp("89000001", Sequence_Id, respMessageBody);
			session.write(IoBuffer.wrap(BdCommonMethod
					.castHexStringToByte(respStr)));
			forinfo.info("请求连接应答：" + respStr);
		}
		if (Command_Id.equals("80000002")) {
			String respMessageBody = dealConnect_release();
			String respStr = resp("89000002", Sequence_Id, respMessageBody);
			session.write(IoBuffer.wrap(BdCommonMethod
					.castHexStringToByte(respStr)));
			forinfo.info("链路拆除应答：" + respStr);
		}
		if (Command_Id.equals("80000003")) {
			String messageBody = hexData.substring(36);
			String respMessageBody = messageBody;
			String respStr = resp("89000003", Sequence_Id, respMessageBody);
			session.write(IoBuffer.wrap(BdCommonMethod
					.castHexStringToByte(respStr)));
			forinfo.info("链路检测应答：" + respStr);
		}
		if (Command_Id.equals("80000006")) {
			String messageBody = hexData.substring(36);
			dealRealtimeData(messageBody);
			String respStr = resp("89000006", Sequence_Id, "");

			session.write(IoBuffer.wrap(BdCommonMethod
					.castHexStringToByte(respStr)));

			forinfo.info("实时性业务应答：" + respStr);
		}
	}

	/**
	 * 处理实时业务数据
	 * 
	 * @param messageBody
	 * @return
	 */
	private void dealRealtimeData(String messageBody) {
		// TODO Auto-generated method stub
		String RealTime_Data_Type = messageBody.substring(0, 8);
		String RealTime_Data_Length = messageBody.substring(8, 12);
		String RealTime_Data_Priority = messageBody.substring(12, 14);
		String Reserved = messageBody.substring(14, 32);
		String RealTime_Data_Content = messageBody.substring(32);
		// 位置信息上报
		if (RealTime_Data_Type.equals("89010060")) {
			// 发信方地址
			String SouceAddr = RealTime_Data_Content.substring(0, 32);
			// 收信方地址
			String DestAddr = RealTime_Data_Content.substring(32, 64);
			// 发送时间
			String ReportTime = RealTime_Data_Content.substring(64, 72);
			// 保留备用
			String Reservedd = RealTime_Data_Content.substring(72, 78);
			// 位置个数
			String PosNum = RealTime_Data_Content.substring(78, 80);
			// 终端位置
			String Position = RealTime_Data_Content.substring(80);

			String fromCard = "";
			if (SouceAddr.startsWith("01")) {
				String SouceAddrCard = SouceAddr.substring(2);
				// 卡号
				fromCard = new String(
						BdCommonMethod.castHexStringToByte(SouceAddrCard))
						.trim();
			}
			Date d = new Date((Long.parseUnsignedLong(ReportTime, 16)) * 1000);
			// 发送时间
			Calendar recivedTime = Calendar.getInstance();
			recivedTime.setTime(d);
			// 定位时间
			String hexLocatime = Position.substring(0, 8);
			String hexLongitude = Position.substring(8, 16);
			String hexLatitude = Position.substring(16, 24);
			String hexHigh = Position.substring(24, 28);
			String hexAzimuth = Position.substring(28, 32);
			String hexSpeed = Position.substring(32, 36);
			String hexSatChoose = Position.substring(36, 38);
			
			String hexPosPrecision = Position.substring(38, 40);//定位精度
			String hexEmergency = Position.substring(40, 42);//是否紧急定位
			String hexMultivalue = Position.substring(42, 44);//是否多值解
			
//			System.out.println("定位精度："+hexPosPrecision+"  紧急定位:"+hexEmergency+" 多值解:"+hexMultivalue);

			Date d1 = new Date((Long.parseUnsignedLong(hexLocatime, 16)) * 1000);
			// 定位时间
			Calendar PosTime = Calendar.getInstance();
			PosTime.setTime(d1);

			String LongitudeStr = BdCommonMethod
					.castHexStringToDcmString(hexLongitude);
			Double Longitude = Double.parseDouble(LongitudeStr) / 1000000;

			String LatitudeStr = BdCommonMethod
					.castHexStringToDcmString(hexLatitude);
			Double Latitude = Double.parseDouble(LatitudeStr) / 1000000;

			String HighStr = BdCommonMethod
					.castHexStringToDcmString(hexHigh);
			Double High = Double.parseDouble(HighStr);

			String AzimuthStr = BdCommonMethod
					.castHexStringToDcmString(hexAzimuth);
			Double Azimuth = Double.parseDouble(AzimuthStr) / 10;

			String SpeedStr = BdCommonMethod.castHexStringToDcmString(hexSpeed);
			Double Speed = Double.parseDouble(SpeedStr) / 10;
			
			 boolean EmergencyLocation=false;
			    if(hexEmergency.equals("01")){
			    	EmergencyLocation=true;
			    }else if(hexEmergency.equals("00")){
			    	EmergencyLocation=false;
			    }
			
			ThCardLocationDto cardLocationDto = new ThCardLocationDto();
			cardLocationDto.setFromCardNumber(fromCard);
			cardLocationDto.setAltitude(High);
			cardLocationDto.setCreatedTime(CalendarToString(PosTime, null));
			cardLocationDto.setDirection(Azimuth);
			cardLocationDto.setLatitude(Latitude);
			cardLocationDto.setLongitude(Longitude);
			cardLocationDto.setSpeed(Speed);
			cardLocationDto.setEmergencyLocation(EmergencyLocation);

			fordebug.debug("卡号：" + cardLocationDto.getFromCardNumber()
					+ ",定位时间：" + cardLocationDto.getCreatedTime() + ",经度："
					+ cardLocationDto.getLongitude() + ",纬度"
					+ cardLocationDto.getLatitude() + ",高度"
					+ cardLocationDto.getAltitude() + ",速度"
					+ cardLocationDto.getSpeed() + ",方向"
					+ cardLocationDto.getDirection()+",紧急定位"
					+cardLocationDto.getEmergencyLocation());

			sendLocationTobagalaxy(cardLocationDto, messageBody);
		}
		// 北斗终端之间的短报文通信信息
		if (RealTime_Data_Type.equals("80010080")) {
			// 过滤重复短报文
			if (dataList.contains(messageBody)) {
				forinfo.info("过滤重复短报文数据：" + messageBody);
				return;
			}
			dataList.add(messageBody);
			if (dataList.size() > 99999) {
				for (int i = 0; i < 1000; i++) {
					dataList.remove(0);
				}
			}
			// 发信方地址
			String SouceAddr = RealTime_Data_Content.substring(0, 32);
			// 收信方地址
			String DestAddr = RealTime_Data_Content.substring(32, 64);
			// 发送时间
			String ReportTime = RealTime_Data_Content.substring(64, 72);
			// 保留备用
			String Reservedd = RealTime_Data_Content.substring(72, 78);
			// 信息长度,单位：字节。1≤Length≤256
			String Length = RealTime_Data_Content.substring(78, 80);
			// 电文形式：0x00表示汉字通信，0x01表示代码通信，0x02表示混合编码通信。
			String ContentType = RealTime_Data_Content.substring(80, 82);
			// 信息内容
			String Content = "";
			try {
				// 按数据长度截取
				/*
				 * Content = RealTime_Data_Content.substring(82, (82 +
				 * (Integer.parseInt(Length, 16))*2));
				 */
				Content = RealTime_Data_Content.substring(82);
			} catch (Exception e) {
				// TODO Auto-generated catch block

				forerror.error("异常数据消息体：" + messageBody);
				forerror.error("异常数据内容：" + RealTime_Data_Content);
				e.printStackTrace();

			}
			String fromCard = "";
			if (SouceAddr.startsWith("01")) {
				String SouceAddrCard = SouceAddr.substring(2);
				// 卡号
				fromCard = new String(
						BdCommonMethod.castHexStringToByte(SouceAddrCard))
						.trim();
			}
			String toCard = "";
			if (DestAddr.startsWith("01")) {
				String DestAddrCard = DestAddr.substring(2);
				// 卡号
				toCard = new String(
						BdCommonMethod.castHexStringToByte(DestAddrCard))
						.trim();
			}
			Date d = new Date((Long.parseUnsignedLong(ReportTime, 16)) * 1000);
			// 发送时间
			Calendar recivedTime = Calendar.getInstance();
			recivedTime.setTime(d);

			String message = "";
			if (ContentType.equals("00")) {
				try {
					message = new String(
							BdCommonMethod.castHexStringToByte(Content), "GBK");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (ContentType.equals("01")) {
				message = Content;
			} else {
				message = new String(
						BdCommonMethod.castHexStringToByte(Content));
			}

			ThCardMessageDto cm = new ThCardMessageDto();
			// cm.setContent(message);Content
			cm.setContent(Content);
			cm.setCreatedTime(CalendarToString(recivedTime, null));
			cm.setFromCardNumber(fromCard);
			cm.setToCardNumber(toCard);

			fordebug.debug("发送卡号：" + cm.getFromCardNumber() + ",接收卡号："
					+ cm.getToCardNumber() + ",消息内容：" + cm.getContent()
					+ ",发送时间：" + cm.getCreatedTime());
			sendMessageTobagalaxy(cm, messageBody);
		}
	}

	/**
	 * 处理链接释放
	 * 
	 * @return
	 */
	private String dealConnect_release() {
		// TODO Auto-generated method stub
		String Status = "00000000";
		String Reserved = "00000000";
		String crs = Status + Reserved;
		return crs;
	}

	/**
	 * 请求连接
	 * 
	 * @param messageBody
	 * @param session
	 */
	private String dealConnect(String messageBody) {
		// TODO Auto-generated method stub
		String Source_Addr = messageBody.substring(0, 16);
		String AuthenticatorSource = messageBody.substring(16, 48);
		String Version = messageBody.substring(48, 50);
		String Reserved = messageBody.substring(50, 56);
		String Timestamp = messageBody.substring(56, 64);

		String Str_Source_Addr = new String(
				BdCommonMethod.castHexStringToByte(Source_Addr));
		String AuthenticatorSource_jy = Source_Addr + "000000000000000000"
				+ EncryptionByMD5.getMD5(SharedSecret.getBytes()) + Timestamp;
		String AuthenticatorSource_jiaoyan = EncryptionByMD5
				.getMD5(BdCommonMethod
						.castHexStringToByte(AuthenticatorSource_jy));

		// 应答状态
		String Status = "00000000";
		if (!Str_Source_Addr.equals(LoginID)) {
			Status = "00000002";
		}

		String ConnectResp = dealConnectResp(Status, AuthenticatorSource,
				Version, Reserved);
		return ConnectResp;

	}

	/**
	 * 处理链接返回
	 * 
	 * @param session
	 */
	private String dealConnectResp(String Status, String AuthenticatorSource,
			String Version, String Reserved) {
		// TODO Auto-generated method stub

		String A3THCENTER = Status + AuthenticatorSource
				+ EncryptionByMD5.getMD5(SharedSecret.getBytes());
		String Authenticator3THCENTER = EncryptionByMD5.getMD5(A3THCENTER
				.toLowerCase().getBytes());

		StringBuilder sb = new StringBuilder("00");
		for (int i = 0; i < 255; i++) {
			sb.append("00");
		}
		Reserved = Reserved + sb.toString();
		String messageBody = Status + Authenticator3THCENTER + Version
				+ Reserved;
		return messageBody;

	}

	/**
	 * 应答返回
	 * 
	 * @return
	 */
	public String resp(String Command_Id, String Sequence_Id, String messagebody) {
		String FixedFlag = "FFFF";
		int str_len = (36 + (messagebody == null ? 0 : messagebody.length())) / 2;// 数据长度
		String Total_Length = BdCommonMethod.castDcmStringToHexString(
				Integer.toString(str_len), 4);
		String CompressionEncryption = "00";
		String Status = "00";
		String Reserved = "0000";
		return FixedFlag + Total_Length + Command_Id + Sequence_Id
				+ CompressionEncryption + Status + Reserved + messagebody;
	};

	public void sendMessageTobagalaxy(ThCardMessageDto cm, String data) {
		Thread thread2 = new Thread() {
			public void run() {
				try {
					HttpClient httpClient = new DefaultHttpClient();
					httpClient.getParams().setParameter(
							CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
					httpClient.getParams().setParameter(
							CoreConnectionPNames.SO_TIMEOUT, 6000);
					HttpPost httpPostReq = new HttpPost(thlmessageurl);
					JSONObject obj = JSONObject.fromObject(cm);

					String aaString = obj.toString();
					StringEntity s = new StringEntity(aaString);
					s.setContentEncoding("UTF-8");
					s.setContentType("application/json");
					httpPostReq.setEntity(s);
					HttpResponse resp = httpClient.execute(httpPostReq);
					if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(resp.getEntity()
										.getContent()));
						StringBuffer result = new StringBuffer();
						String inputLine = null;
						while ((inputLine = reader.readLine()) != null) {
							result.append(inputLine);
						}
						forinfo.info("成功转发缓存数据到平台4.0：" + cm.toString());

					} else {
						forerror.error("返回错误：转发数据到平台4.0：");
					}

				} catch (Exception e) {
					forerror.error("错误：转发数据到平台4.0： 错误详情：" + e.getStackTrace());
					Linedata ld = new Linedata();
					ld.setStrdata(data);
					ld.setCreatedTime(Calendar.getInstance());
				}
			}

		};
		thread2.start();
	}

	// 发送定位到北斗平台
	public void sendLocationTobagalaxy(ThCardLocationDto cl, String data) {
		Thread thread2 = new Thread() {
			public void run() {
				try {
					HttpClient httpClient = new DefaultHttpClient();
					httpClient.getParams().setParameter(
							CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
					httpClient.getParams().setParameter(
							CoreConnectionPNames.SO_TIMEOUT, 6000);
					HttpPost httpPostReq = new HttpPost(thlocationurl);
					JSONObject obj = JSONObject.fromObject(cl);
					String aaString = obj.toString();
					StringEntity s = new StringEntity(aaString);
					s.setContentEncoding("UTF-8");
					s.setContentType("application/json");
					httpPostReq.setEntity(s);
					HttpResponse resp = httpClient.execute(httpPostReq);
					if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(resp.getEntity()
										.getContent()));
						StringBuffer result = new StringBuffer();
						String inputLine = null;
						while ((inputLine = reader.readLine()) != null) {
							result.append(inputLine);
						}
						forinfo.info("成功转发缓存数据到平台4.0：" + cl.toString());

					} else {
						forerror.error("返回错误：转发数据到平台4.0：");
					}

				} catch (Exception e) {
					forerror.error("错误：转发数据到平台4.0： 错误详情：" + e.getStackTrace());

					Linedata ld = new Linedata();
					ld.setStrdata(data);
					ld.setCreatedTime(Calendar.getInstance());
					linedataBiz.saveLinedata(ld);// 成功保存
				}
			}

		};
		thread2.start();
	}

	// 发送数据库中的缓存
	public void sendDbMsg() {
		Thread thread2 = new Thread() {
			public void run() {
				while (true) {
					List<Linedata> linecmdList = linedataBiz.getLinedataList(1,
							10);
					if (linecmdList != null && linecmdList.size() > 0) {
						for (Linedata linedata : linecmdList) {
							linedataBiz.delete(linedata.getId());
							dealRealtimeData(linedata.getStrdata());
						}
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
		};
		thread2.start();
	}

	public static void main(String[] args) {
		testHttpclient();
	}

	public static void testHttpclient() {
		CardMessageDto cm = new CardMessageDto();
		cm.setContent("你好");
		cm.setCreatedTime(Calendar.getInstance());
		cm.setFromCardNumber("307175");
		cm.setToCardNumber("307175");
		try {
			String uri = "http://127.0.0.1:8081/bdcbus/bdcbus/pullBdObject.do";
			HttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 6000);
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			// obj.put("bdChannel", "DEVICE_40");
			obj.put("bdChannel", "DEVICE_TIANHUI");
			obj.put("obj", cm);

			String aaString = obj.toString();
			StringEntity s = new StringEntity(aaString);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				forinfo.info("成功转发缓存数据到平台4.0：" + cm.toString());

			} else {
				forerror.error("返回错误：转发数据到平台4.0：");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calendar时间转换为String时间
	 * 
	 * @param calendar
	 * @param format
	 * @return
	 */
	private static String CalendarToString(Calendar calendar, String format) {
		if (calendar == null) {
			calendar = Calendar.getInstance();
		}
		if (format == null) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(calendar.getTime());
		return dateStr;
	}

}
