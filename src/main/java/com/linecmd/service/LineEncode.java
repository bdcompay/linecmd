package com.linecmd.service;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class LineEncode extends ProtocolEncoderAdapter {

	@Override
	public void encode(IoSession ioSession, Object message, ProtocolEncoderOutput arg2)
			throws Exception {
		// TODO Auto-generated method stub
		IoBuffer ioBuffer = (IoBuffer) message;
		byte[] bytedata = ioBuffer.array();
		arg2.write(bytedata);
		System.out.print(bytedata);
		
	}

}
