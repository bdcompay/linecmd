package com.linecmd.service;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.linecmd.entity.Linedata;
import com.pro.Analysis;


@Component
public class LineHandler extends IoHandlerAdapter {
	private static Logger forinfo = Logger.getLogger("forinfo");
	
	@Autowired
	private SmackHandler smackHandler;
	@Autowired
	private LinedataService linedataService;
	@Value("${test.send}")
	private boolean testsend;
	
	/**
	 * 保存北斗数据包序号，用于过滤重复的北斗数据
	 */
	public static List<String> list = new ArrayList<String>();
	
	@Override
	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(session, cause);
	}

	@Override
	public void messageReceived(IoSession session, Object message)
			throws Exception {
		// TODO Auto-generated method stub
		IoBuffer ioBuffer = (IoBuffer) message;
		byte[] bytedata = ioBuffer.array();
		//byte[] bpcDatas = dealrevdata(bytedata);
		
		//String格式的北斗数据
		String dataStr =  Analysis.ByteHexToString(bytedata, bytedata.length);
		
		forinfo.info("收到总站数据："+ dataStr);
		
		//北斗数据包序号
		String numberOrder = dataStr.substring(4, 12);
		if(!dataFilter(numberOrder)) return ;
		
		// 转发到4.0平台
		smackHandler.sendLinedata(Analysis
						.ByteHexToString(bytedata, bytedata.length));
		// 转发到4.0平台测试平台
		if(testsend)
		smackHandler.sendLinedataTtest(Analysis
				.ByteHexToString(bytedata, bytedata.length));
	}

	/**
	 * 专线协议转成bpc的协议
	 * @param revdata
	 * @return
	 */
	public byte[] dealrevdata(byte[] revdata) {
		byte[] bdata = new byte[1024];
		bdata[0] = (byte) 0x0F;
		bdata[1] = (byte) 0xFC;
		bdata[2] = (byte) 0x01;
		bdata[3] = (byte) 0x01;
		bdata[4] = (byte) 0x01;
		bdata[5] = (byte) 0x03;
		bdata[6] = (byte) 0x00;
		int iSite = 7;
		for (int i = 0; i < revdata.length; i++) {
			switch (revdata[i]) {
			case (byte) 0x0F:
				bdata[iSite] = (byte) 0x7E;
				iSite++;
				bdata[iSite] = (byte) 0x01;
				iSite++;
				continue;
			case (byte) 0xFF:
				bdata[iSite] = (byte) 0x7E;
				iSite++;
				bdata[iSite] = (byte) 0x02;
				iSite++;
				continue;
			case (byte) 0x7E:
				bdata[iSite] = (byte) 0x7E;
				iSite++;
				bdata[iSite] = (byte) 0x03;
				iSite++;
				continue;
			default:
				bdata[iSite] = revdata[i];
				iSite++;
				continue;
			}
		}
		bdata[iSite] = (byte) 0xFF;
		iSite++;
		byte[] bdata1 = new byte[iSite];
		for (int j = 0; j < iSite; j++) {
			bdata1[j] = bdata[j];
		}
		revdata = null;
		bdata = null;
		return bdata1;

	}
	
	/**
	 * 通过包序号的方式过滤重复的北斗数据，判断北斗数据包序号是否有重复
	 * 
	 * @param numberOrder
	 * 		北斗数据包序号
	 * @return
	 */
	private boolean dataFilter(String numberOrder){
		synchronized (numberOrder) {
			if(!list.contains(numberOrder)){
				if(list.size()>10000){
					list.remove(0);
				}
				list.add(numberOrder);
				return true;
			}
			return false;
		}
	}

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		// TODO Auto-generated method stub
		super.messageSent(session, message);
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionClosed(session);
	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionCreated(session);
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status)
			throws Exception {
		// TODO Auto-generated method stub
		super.sessionIdle(session, status);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		super.sessionOpened(session);
	}

}
