package com.linecmd.service;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.pro.Analysis;

public class ThDecode extends CumulativeProtocolDecoder {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	
	
	@Override
	protected boolean doDecode(IoSession arg0, IoBuffer in,
			ProtocolDecoderOutput out) throws Exception {
		// TODO Auto-generated method stub
		int count = in.remaining();
		if (count > 0) {// 有数据时
			byte[] revdata = new byte[count];
			in.get(revdata);
			out.write(IoBuffer.wrap(revdata));
		}
		return false;
	}

	

}
