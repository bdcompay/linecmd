package com.linecmd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linecmd.dao.LinedataDao;
import com.linecmd.entity.Linedata;

@Service
public class LinedataService {
	@Autowired
	private LinedataDao linedataDao;

	public void saveLinedata(Linedata linedata){
		linedataDao.save(linedata);
	}
	
	public void deleteLinedata(Linedata linedata){
		linedataDao.delete(linedata.getId());
	}
	
	public List<Linedata> getLinedataList(int page,int pageSize){
		return linedataDao.getLinedataList(page, pageSize);
	}
	
	public void updateLinedata(Linedata linedata){
		linedataDao.update(linedata);
	}
}