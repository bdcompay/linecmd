package com.linecmd.service;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.pro.Analysis;

public class LineDecode extends CumulativeProtocolDecoder {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	
	
	@Override
	protected boolean doDecode(IoSession arg0, IoBuffer in,
			ProtocolDecoderOutput out) throws Exception {
		// TODO Auto-generated method stub
		int count = in.remaining();
		if (count > 0) {// 有数据时
			byte[] revdata = new byte[count];
			in.get(revdata);
//			forinfo.info("收到总站数据："
//					+ Analysis.ByteHexToString(revdata, revdata.length));
			/*String s= Analysis.ByteHexToString(revdata, revdata.length);
			if(s.contains("0200BC")||s.contains("0200bc")){
				fordebug.debug(s);
			};*/
			out.write(IoBuffer.wrap(revdata));
		}
		return false;
	}

	/**
	 * 我们自己的协议转成专线协议
	 * 
	 * @param b_rev
	 * @return
	 */
	public byte[] bdataChangelinedata(byte[] b_rev) {
		byte[] bRetrunData = null;
		byte[] bdata = new byte[1024];
		int y = 0;
		for (int i = 7; i < b_rev.length - 1; i++) {
			if (b_rev[i] == (byte) 0x7E) {
				if (b_rev[i + 1] == (byte) 0x01) {
					bdata[y] = 0x0F;
					i++;
					y++;
				} else if (b_rev[i + 1] == (byte) 0x02) {
					bdata[y] = (byte) 0xFF;
					i++;
					y++;
				} else if (b_rev[i + 1] == (byte) 0x03) {
					bdata[y] = (byte) 0x7E;
					i++;
					y++;
				}
			} else {
				bdata[y] = b_rev[i];
				y++;
			}
		}
		if (y > 0) {
			bRetrunData = new byte[y];
			for (int k = 0; k < y; k++)
				bRetrunData[k] = bdata[k];

			return bRetrunData;
		} else {
			return null;
		}
	}

}
