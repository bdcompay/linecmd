
package com.linecmd.service.dto;

import java.io.Serializable;
import java.util.Calendar;

public class ThCardMessageDto
    implements Serializable
{

    private static final long serialVersionUID = 1L;
    private long id;
    private Integer msgId;
    private String fromCardNumber;
    private String toCardNumber;
    private String content;
    private String createdTime;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Integer getMsgId() {
		return msgId;
	}
	public void setMsgId(Integer msgId) {
		this.msgId = msgId;
	}
	public String getFromCardNumber() {
		return fromCardNumber;
	}
	public void setFromCardNumber(String fromCardNumber) {
		this.fromCardNumber = fromCardNumber;
	}
	public String getToCardNumber() {
		return toCardNumber;
	}
	public void setToCardNumber(String toCardNumber) {
		this.toCardNumber = toCardNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
}

