
package com.linecmd.service.dto;

import java.io.Serializable;
import java.util.Calendar;

public class ThCardLocationDto
    implements Serializable
{
    private static final long serialVersionUID = 1L;
    private long id;
    private String fromCardNumber;
    private Double longitude;
    private Double latitude;
    private Double altitude;
    private Double speed;
    private Double direction;
    private String createdTime;
    private Boolean emergencyLocation;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFromCardNumber() {
		return fromCardNumber;
	}
	public void setFromCardNumber(String fromCardNumber) {
		this.fromCardNumber = fromCardNumber;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getAltitude() {
		return altitude;
	}
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Double getDirection() {
		return direction;
	}
	public void setDirection(Double direction) {
		this.direction = direction;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	
	 
	public Boolean getEmergencyLocation() {
		return emergencyLocation;
	}
	public void setEmergencyLocation(Boolean emergencyLocation) {
		this.emergencyLocation = emergencyLocation;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
}


