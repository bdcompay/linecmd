package com.linecmd.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.annotation.PostConstruct;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.linecmd.biz.LinedataBiz;
import com.linecmd.entity.Linedata;

@Component
public class SmackHandler {
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger forerror = Logger.getLogger("forerror");
	public static Vector<Linedata> cacheLinedataVector = new Vector<Linedata>();
	@Value("${cacheSize}")
	private int cacheSize;
	@Autowired
	private LinedataService linedataService;
	@Value("${bdcbus.url}")
	private String uri;
	@Value("${testbdcbus.url}")
	private String testurl;

	static int idd = 0;

	public static int sh = 0;
	
	@Autowired
	private LinedataBiz linedataBiz;
	
	

	@PostConstruct
	public void init() {
		saMsgToDB();
		/*saveMsgToDB();
		sendCaheMsg();
		sendDbMsg();*/
	}

	// 发送内存中的缓存
	public void sendCaheMsg() {
		Thread thread = new Thread() {
			@SuppressWarnings("deprecation")
			public void run() {
				while (true) {
					if (!cacheLinedataVector.isEmpty()
							&& cacheLinedataVector.size() > cacheSize) {
						while (cacheLinedataVector.iterator().hasNext()) {
							Linedata linedata = cacheLinedataVector.iterator()
									.next();
							try {
								HttpClient httpClient = new DefaultHttpClient();
								httpClient
										.getParams()
										.setParameter(
												CoreConnectionPNames.CONNECTION_TIMEOUT,
												6000);
								httpClient.getParams().setParameter(
										CoreConnectionPNames.SO_TIMEOUT, 6000);
								HttpPost httpPostReq = new HttpPost(uri);
								JSONObject obj = new JSONObject();
								// obj.put("bdChannel", "DEVICE_40");
								obj.put("bdChannel", "DEVICE_LINE");
								obj.put("dataStr", linedata.getStrdata());
								String aaString = obj.toString();
								StringEntity s = new StringEntity(aaString);
								s.setContentEncoding("UTF-8");
								s.setContentType("application/json");
								httpPostReq.setEntity(s);
								HttpResponse resp = httpClient
										.execute(httpPostReq);
								if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
									BufferedReader reader = new BufferedReader(
											new InputStreamReader(resp
													.getEntity().getContent()));
									StringBuffer result = new StringBuffer();
									String inputLine = null;
									while ((inputLine = reader.readLine()) != null) {
										result.append(inputLine);
									}
									forinfo.info("成功转发缓存数据到平台4.0："
											+ linedata.getStrdata());
									cacheLinedataVector.remove(linedata);
								} else {
									forerror.error("返回错误：转发数据到平台4.0："
											+ linedata.getStrdata());
								}

							} catch (Exception e) {
								forerror.error("错误：转发数据到平台4.0："
										+ linedata.getStrdata() + " 错误详情："
										+ e.getStackTrace());
							}
						}
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						try {
							Thread.sleep(60000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}

		};
		thread.start();
	}

	// 内存太多转存数据库
	public void saMsgToDB() {
		Thread thread2 = new Thread() {
			public void run() {
				while (true) {
					if (cacheLinedataVector.size() > cacheSize) {
						for (int i = 0; i < 1000; i++) {
							while (cacheLinedataVector.iterator().hasNext()) {
								Linedata linedata = cacheLinedataVector
										.iterator().next();
								linedataBiz.saveLinedata(linedata);
								cacheLinedataVector.remove(linedata);
								try {
									if (cacheLinedataVector.size() > cacheSize) {
										Thread.sleep(100);
									} else {
										Thread.sleep(60000);
									}

								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}

					}
				}
			};
		};
		thread2.start();
	}

	// 发送数据库中的缓存
	public void sendDbMsg() {
		Thread thread2 = new Thread() {
			public void run() {
				while (true) {
					List<Linedata> linecmdList = linedataBiz
							.getLinedataList(1, 100);
					if (linecmdList != null && linecmdList.size() > 0) {
						for (Linedata linedata : linecmdList) {
							try {
								HttpClient httpClient = new DefaultHttpClient();
								httpClient
										.getParams()
										.setParameter(
												CoreConnectionPNames.CONNECTION_TIMEOUT,
												6000);
								httpClient.getParams().setParameter(
										CoreConnectionPNames.SO_TIMEOUT, 6000);
								HttpPost httpPostReq = new HttpPost(uri);
								JSONObject obj = new JSONObject();
								// obj.put("bdChannel", "DEVICE_40");
								obj.put("bdChannel", "DEVICE_LINE");
								obj.put("dataStr", linedata.getStrdata());
								String aaString = obj.toString();
								StringEntity s = new StringEntity(aaString);
								s.setContentEncoding("UTF-8");
								s.setContentType("application/json");
								httpPostReq.setEntity(s);
								HttpResponse resp = httpClient
										.execute(httpPostReq);
								if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
									BufferedReader reader = new BufferedReader(
											new InputStreamReader(resp
													.getEntity().getContent()));
									StringBuffer result = new StringBuffer();
									String inputLine = null;
									while ((inputLine = reader.readLine()) != null) {
										result.append(inputLine);
									}
									forinfo.info("成功转发缓存数据到平台4.0："
											+ linedata.getStrdata());
									linedataBiz.deleteLinedata(linedata);
									;

								} else {
									forerror.error("返回错误：转发数据到平台4.0："
											+ linedata.getStrdata());
								}

							} catch (Exception e) {
								forerror.error("错误：转发数据到平台4.0："
										+ linedata.getStrdata() + " 错误详情："
										+ getTrace(e));
							}
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		};
		thread2.start();
	}

	// 直接转发数据
	public void sendLinedata(final String strlinedata) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 6000);
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			// obj.put("bdChannel", "DEVICE_40");
			obj.put("bdChannel", "DEVICE_LINE");
			obj.put("dataStr", strlinedata);
			String aaString = obj.toString();
			StringEntity s = new StringEntity(aaString);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				forinfo.info("成功转发数据到平台4.0：" + strlinedata);
				Linedata ld = new Linedata();
				ld.setStrdata(strlinedata);
				ld.setCreatedTime(Calendar.getInstance());
				linedataBiz.saveLinedata(ld);//成功保存

			} else {
				forerror.error("返回错误：转发数据到平台4.0：" + strlinedata);
				Linedata ld = new Linedata();
				ld.setStrdata(strlinedata);
				ld.setCreatedTime(Calendar.getInstance());
//				linedataBiz.saveLinedata(ld);
				cacheLinedataVector.add(ld);
			}

		} catch (Exception e) {
			forerror.error("错误：转发数据到平台4.0：" + strlinedata + " 错误详情："
					+ getTrace(e));
			Linedata ld = new Linedata();
			ld.setStrdata(strlinedata);
			ld.setCreatedTime(Calendar.getInstance());
			
			cacheLinedataVector.add(ld);
		}
	}

	// 直接转发到测试平台
	public void sendLinedataTtest(final String strlinedata) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 6000);
			HttpPost httpPostReq = new HttpPost(testurl);
			JSONObject obj = new JSONObject();
			// obj.put("bdChannel", "DEVICE_40");
			obj.put("bdChannel", "DEVICE_LINE");
			obj.put("dataStr", strlinedata);
			String aaString = obj.toString();
			StringEntity s = new StringEntity(aaString);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				forinfo.info("成功转发数据到测试平台4.0：" + strlinedata);

			} else {
				// forerror.error("返回错误：转发数据到平台4.0：" + strlinedata);
			}

		} catch (Exception e) {
			// forerror.error("错误：转发数据到平台4.0：" + strlinedata + " 错误详情："
			// + getTrace(e));
		}
	}

	public String getTrace(Throwable t) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		t.printStackTrace(writer);
		StringBuffer buffer = stringWriter.getBuffer();
		return buffer.toString();
	}

}
