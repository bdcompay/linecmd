package com.linecmd.service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.transport.socket.DatagramSessionConfig;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LinecmdAsServer {
	private static Logger forinfo = Logger.getLogger("forinfo");
	@Autowired
	private IoHandler lineHandler;
	@Autowired
	private THDataHandler tHDataHandler;
	@Value("${threadCounts}")
	private int threadCounts;
	@Value("${port}")
	private int port;
	
	@PostConstruct
	public void init() {
		forinfo.info("启动监听端口："+port);
		System.out.println("启动监听端口："+port);
		ThDecode td=new ThDecode();
		startTCPServer(tHDataHandler,threadCounts,port,td);
		
	}
	
	public void startServer(IoHandler ioHandler,int threadCounts,int port) {
		// 创建了一个NIO的数据报接收器NioDatagramAcceptor对象
		NioDatagramAcceptor acceptor = new NioDatagramAcceptor();
//		DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();
//		chain.addLast("logger", new LoggingFilter());
		// 编码器
		LineEncode encoder = new LineEncode();
		// 解码器
		LineDecode decode = new LineDecode();
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(encoder, decode));// 指定编码过滤器
		// 绑定处理器
		acceptor.setHandler(ioHandler);
		Executor threadpool = Executors.newFixedThreadPool(threadCounts);
		acceptor.getFilterChain().addLast("exector",
				new ExecutorFilter(threadpool));
		DatagramSessionConfig dcfg = acceptor.getSessionConfig();// 建立连接的配置文件
		dcfg.setReadBufferSize(4096);// 设置接收最大字节默认4096
		dcfg.setReceiveBufferSize(102400000);// 设置输入缓冲区的大小
		dcfg.setSendBufferSize(1024);// 设置输出缓冲区的大小
		dcfg.setReuseAddress(true);// 设置每一个非主监听连接的端口可以重用
		try {
			acceptor.bind(new InetSocketAddress(port));// 绑定端口
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param ioHandler
	 * @param threadCounts
	 * @param port
	 * @param obj
	 */
	public void startTCPServer(IoHandler ioHandler,int threadCounts,int port,CumulativeProtocolDecoder decode) {
		// 创建了一个NIO的数据报接收器NioDatagramAcceptor对象
		NioSocketAcceptor acceptor = new NioSocketAcceptor();
//		NioDatagramAcceptor acceptor = new NioDatagramAcceptor();
//		DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();
//		chain.addLast("logger", new LoggingFilter());
		// 编码器
		LineEncode encoder = new LineEncode();
		
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(encoder, decode));// 指定编码过滤器
		// 绑定处理器
		acceptor.setHandler(ioHandler);
		Executor threadpool = Executors.newFixedThreadPool(threadCounts);
		acceptor.getFilterChain().addLast("exector",
				new ExecutorFilter(threadpool));
		SocketSessionConfig dcfg = acceptor.getSessionConfig();// 建立连接的配置文件
		dcfg.setReadBufferSize(4096);// 设置接收最大字节默认4096
		dcfg.setReceiveBufferSize(1024);// 设置输入缓冲区的大小
		dcfg.setSendBufferSize(1024);// 设置输出缓冲区的大小
		dcfg.setReuseAddress(true);// 设置每一个非主监听连接的端口可以重用
		try {
			acceptor.bind(new InetSocketAddress(port));// 绑定端口
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
